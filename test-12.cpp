/* Program to execute the join() in thread */

#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
using namespace std;

void *thread_fnc(void *arg);
char thread_msg[] = "Hello Thread !";

int main(int argc, char **argv)
{
	pthread_t my_thread;
	void *ret_join;
	int ret = pthread_create(&my_thread, NULL, thread_fnc, (void*) thread_msg);
	if (ret != 0)
	{
		perror("pthread_create failed");
		exit(EXIT_FAILURE);
	}
	cout<<"Waiting for the thread to finish !"<<endl;
	ret = pthread_join(my_thread, &ret_join);
	if (ret != 0)
	{
		perror("pthread_join failed");
		exit(EXIT_FAILURE);
	}
	cout<<"Thread joined, it returned"<<(char *) ret_join;
	cout<<"New thread message: "<<thread_msg;
	cout<<endl;
	exit(EXIT_SUCCESS);
	return 0;
}

void *thread_fnc(void *arg)
{
	cout<<"This is a thread func(), arg is: "<<(char *)arg<<endl;
	strcpy(thread_msg, "Bye !");
	pthread_exit(NULL);
}
