/* Program to create multiple threads */
#include <pthread.h>
#include <iostream>
#include <stdlib.h>

using namespace std;
#define NUM_THREADS 5

// pthread_create(thread, attr, start_routine, arg);

// thread -> An opaque, unique identifier for the new thread subroutine
// attr -> An opaque attribute object that mey be used to set thread attributes. You can specify
//         a thread attribute object, or NULL for the default values
//         e.g, pthread_attr_init(), pthread_attr_destroy()
// start_routine -> The C++ routine that the thread will execute once it is created
// arg -> A singlr argument that may be passed by ref. as a pointer cast of type void

void *worker_thread(void *arg)
{
	cout<<"This is a worker_thread() - #"<<(long)arg<<endl;
	pthread_exit(NULL);
}

int main(int argc, char **argv)
{
	pthread_t my_thread[NUM_THREADS];
	long id;
	cout<<"In main: creating threads"<<endl;
	for(id = 1; id <= NUM_THREADS; id++)
	{
		int ret = pthread_create(&my_thread[id], NULL, &worker_thread, (void*)id);
		if (ret != 0)
		{
			cout<<"Error: pthread_create() failed"<<endl;
			exit(EXIT_FAILURE);
		}
	}
	pthread_exit(NULL);
	return 0;
}
