/* Semaphores with empty and full */
#include <iostream>
#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <stdlib.h>

#define MAX 1

using namespace std;
int buffer[MAX];
int fill = 0;
int use = 0;

void put(int value)
{
	buffer[fill] = value;
	fill = (fill + 1) % MAX;
}

int get()
{
	int b = buffer[use];
	use = (use + 1) % MAX;
	return b;
}

int loops = 0;

sem_t empty;
sem_t full;

void *producer(void *arg)
{
	int i;
	for(i = 0; i < loops; i++)
	{
		sem_wait(&empty);
		put(i);
		sem_post(&full);
	}
}

void *consumer(void *arg)
{
	int i;
	for(i = 0; i < loops; i++)
	{
		sem_wait(&full);
		int b = get();
		sem_post(&empty);
		cout<<b<<endl;
	}
}

int main(int argc, char **argv)
{
	if(argc < 2)
	{
		cout<<"Needs 2nd arg for loop count var";
		return 1;
	}
	loops = atoi(argv[1]);
	sem_init(&empty, 0, MAX);
	sem_init(&full, 0, 0);
	
	pthread_t pThread, cThread;
	pthread_create(&pThread, 0, producer, 0);
	pthread_create(&pThread, 0, consumer, 0);
	pthread_join(pThread, NULL);
	pthread_join(cThread, NULL);
	return 0;
}
